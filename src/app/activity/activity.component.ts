import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { Observable } from "tns-core-modules/data/observable";
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { getJSON, HttpResponse } from "tns-core-modules/http";

import { Proposal, ActivityService } from "./activity.service";

@Component({
	selector: "Activity",
	moduleId: module.id,
	templateUrl: "./activity.component.html",
	styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {
	isDrawerClosed = true;
	proposals: Array<Proposal>;
	private _mainContentText: string;

	@ViewChild(RadSideDrawerComponent, { static: false }) public drawerComponent: RadSideDrawerComponent;
	private drawer: RadSideDrawer;

	constructor(
		private activityService: ActivityService,
		private _changeDetectionRef: ChangeDetectorRef
	) { }

	ngAfterViewInit() {
		this.drawer = this.drawerComponent.sideDrawer;
		this._changeDetectionRef.detectChanges();
	}

	ngOnInit(): void {
		this.getProposals();
		this.proposals = this.activityService.getProposals();
		this.mainContentText = "SideDrawer for NativeScript can be easily setup in the HTML definition of your page by defining tkDrawerContent and tkMainContent. The component has a default transition and position and also exposes notifications related to changes in its state. Swipe from left to open side drawer.";
	}

	getProposals() {
		getJSON("https://jsonplaceholder.typicode.com/posts").then((r: any) => {
		}, (e) => {
			console.log(e)
		});
	}

	onProposalClick(args: Proposal) { }

	get mainContentText() {
		return this._mainContentText;
	}

	set mainContentText(value: string) {
		this._mainContentText = value;
	}

	public openDrawer() {
		if (this.isDrawerClosed) {
			this.drawer.showDrawer();
			this.isDrawerClosed = false;
		} else {
			this.onCloseDrawerTap();
		}
	}

	public onCloseDrawerTap() {
		this.drawer.closeDrawer();
		this.isDrawerClosed = true;
	}
}


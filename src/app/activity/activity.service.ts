import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root"
})

export class ActivityService {
    proposals = new Array<Proposal>(
        { id: 1, name: "Proposal 1" },
        { id: 2, name: "Proposal 2" },
        { id: 3, name: "Proposal 3" },
        { id: 4, name: "Proposal 4" },
        { id: 5, name: "Proposal 5" },
    );

    getProposals(): Array<Proposal> {
        return this.proposals;
    }

    getProposal(id: number): Proposal {
        return this.proposals.filter((proposal) => proposal.id === id)[0];
    }
}

export class Proposal {
    constructor(public id: number, public name: string) { }
}
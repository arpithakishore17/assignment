import { Component, OnInit } from "@angular/core";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
    creadentials = { username: '', password: '' };
    //[nsRouterLink]="['/activity-view']" 
    constructor() { }

    ngAfterViewInit() {}

    onLogin(): void {
        console.log("Button was pressed");
    }

    ngOnInit(): void {}
}

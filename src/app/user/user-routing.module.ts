import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { UserComponent } from "./user.component";

const routes: Routes = [
	{ path: "", component: UserComponent }
]

@NgModule({
	imports: [
		NativeScriptRouterModule.forChild(routes)
	],
	declarations: [],
	schemas: [
		NO_ERRORS_SCHEMA
	]
})
export class UserRoutingModule { }
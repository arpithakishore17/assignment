import { Component, OnInit } from "@angular/core";

@Component({
	selector: "User",
	moduleId: module.id,
	templateUrl: "./user.component.html",
	styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

	constructor() {
	}

	ngOnInit(): void {
	}
}
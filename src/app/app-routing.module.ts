import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "user", loadChildren: () => import("./user/user.module").then(u => u.UserModule) },
    { path: "home", loadChildren: () => import("./home/home.module").then(m => m.HomeModule) },
    { path: "activity-view", loadChildren: () => import("./activity/activity.module").then(a => a.ActivityModule) },
    { path: "create-proposal", loadChildren: () => import("./proposal/proposal.module").then(p => p.ProposalModule) },
    { path: "edit-proposal/:id", loadChildren: () => import("./proposal/proposal.module").then(p => p.ProposalModule) }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
	selector: "Proposal",
	moduleId: module.id,
	templateUrl: "./proposal.component.html",
	styleUrls: ['./proposal.component.css']
})
export class ProposalComponent implements OnInit {
	buttonLabel = 'Create Proposal';
	proposal: Proposal = { id: null, name: '' };
	constructor(
		private activatedRoute: ActivatedRoute,
		private routerExtensions: RouterExtensions
	) { }

	ngOnInit(): void {
		if (this.activatedRoute.snapshot.paramMap.get('id')) {
			this.buttonLabel = 'Update Proposal';
			this.proposal = {
				id: Number(this.activatedRoute.snapshot.paramMap.get('id')),
				name: `Proposal${this.activatedRoute.snapshot.paramMap.get('id')}`
			}
		}
	}

	createProposal() {
		this.routerExtensions.navigate(['/activity-view']);
	}
}


class Proposal {
	constructor(public id: number, public name: string) { }
}